"use strict";
import chai from "chai";
import { expect } from "chai";
import sinon from "sinon";
import sinonChai from "sinon-chai";
import request from 'supertest';
import express from 'express'

import { compose, prop, assoc } from 'ramda';
import initialState from '../src/constants/initial-state';
import rnd from '../src/utils/random'
import { zeroToOne, zeroToTen } from '../src/mutations/check-pet/helpers/random-seed'
import isOne from '../src/utils/predicate/is-one'
import { asleepOrAwake } from '../src/mutations/check-pet/helpers/if-sleepy-update-state'

describe('Routes', () => {
  let params, app, seed, boolean

  beforeEach('import app', () => {
    app = require('../src/index');
  })

  describe('Feed', () => {
    beforeEach('set seed', () => {
      seed = rnd.genCsSeed()
      boolean = compose(
        isOne,
        prop('value'),
        zeroToOne
      )(seed)
    })

    beforeEach('set params', () => {
      params = {
        state: initialState,
        seed: seed,
        time: 100,
        foodChoice: 'snack'
      }
    })

    it('should return res', done => {
      request(app.listen())
      .post('/feed')
      .send(params)
      .set('Accept', 'application/json')
      .expect(200)
      .then(response => {
        expect(
          response.body
        ).to.eql({ 
          health: 100,
          fullness: (100 - zeroToTen(seed).value),
          age: 100,
          stage: 'Baby',
          state: asleepOrAwake(boolean),
          stoolCounter: 1,
          needToDefecate: true,
          isSleepy: boolean
        })
      })
      .finally(() => {
        done()
      })
    })
  })

  describe('Sleep', () => {
    beforeEach('set params', () => {
      params = {
        state: assoc('state', 'Awake', initialState),
        time: 100,
      }
    })

    it('should return res', done => {
      request(app.listen())
      .post('/sleep')
      .send(params)
      .set('Accept', 'application/json')
      .expect(200)
      .then(response => {
        expect(
          response.body
        ).to.eql({ 
          health: 100,
          fullness: 100,
          age: 100,
          stage: 'Baby',
          state: 'Asleep',
          stoolCounter: 0 })
       })
      .finally(() => {
        done()
      })
    })
  })

  describe('Check Status', () => {
    beforeEach('set seed', () => {
      seed = rnd.genCsSeed()
      boolean = compose(
        isOne,
        prop('value'),
        zeroToOne
      )(seed)
    })

    beforeEach('set params', () => {
      params = {
        state: initialState,
        seed: seed,
        time: 2000
      }
    })

    it('should return res', done => {
      request(app.listen())
      .post('/check_status')
      .send(params)
      .set('Accept', 'application/json')
      .expect(200)
      .then(response => {
        expect(
          response.body
        ).to.eql({
          stage: 'Death'
        })
      })
      .finally(() => {
        done()
      })
    })
  })
})