"use strict";
import chai from "chai";
import { expect } from "chai";
import sinon from "sinon";
import sinonChai from "sinon-chai";
import { prop, assoc, propOr, compose } from 'ramda';

import { zeroToOne } from '../src/mutations/check-pet/helpers/random-seed'
import isOne from '../src/utils/predicate/is-one'
import initialState from '../src/constants/initial-state'
import checkPet from '../src/mutations/check-pet'

import rnd from '../src/utils/random'

chai.should();
chai.use(sinonChai);

describe('Check Pet', () => {
  let pet, newPet, seed

  beforeEach('set pet', () => {
    pet = assoc('fullness', 10, initialState)
  })

  beforeEach('set seed', () => {
    seed = rnd.genCsSeed()
  })

  context('when checking Pet', () => {
    context('when pet is alive', () => {
      context('when hungry', () => {

        beforeEach('checkPet', () => {
          newPet = checkPet(seed)(pet)
        })

        it('should have added isHungry flag', () => {
          expect(prop('isHungry', newPet)).to.eql(true)
        })

        it('should have decreased health', () => {
          expect(prop('health', newPet)).to.eql(prop('health', pet) - 10)
        })
      })

      context('when need to defecate', () => {
        beforeEach('set fullness', () => {
          newPet = assoc('fullness', 80, initialState)
        })

        beforeEach('checkPet', () => {
          newPet = checkPet(seed)(newPet)
        })

        it('should have incremented stoolCount', () => {
          expect(prop('stoolCounter', newPet)).to.eql(1)
        })
      })

      context('when not sleepy', () => {
        beforeEach('checkPet', () => {
          newPet = checkPet(seed)(pet)
        })

        it('should be sleepy', () => {
          expect(propOr(false,'isSleepy', newPet)).to.eql(
            compose(
              isOne,
              prop('value'),
              zeroToOne
            )(seed)
          )
        })
      })
    })

    context('when pet is dead', () => {
      beforeEach('set pet', () => {
        pet = assoc('health', 10, pet)
      })

      beforeEach('checkPet', () => {
        pet = checkPet(seed)(pet)
      })

      it('should have stage as dead', () => {
        expect(prop('stage', pet)).to.eql('Dead')
      })
    })
  })
})