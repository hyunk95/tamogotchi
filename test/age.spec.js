"use strict";
import chai from "chai";
import { expect } from "chai";
import sinon from "sinon";
import sinonChai from "sinon-chai";
import { prop } from 'ramda';

import initialState from '../src/constants/initial-state'
import age from '../src/mutations/age'

chai.should();
chai.use(sinonChai);

describe('Age', () => {
  let pet, ageVal

  beforeEach('set pet', () => {
    pet = initialState
  })

  beforeEach('set value', () => {
    ageVal = 100
  })

  context('when adding age', () => {
    beforeEach('add age', () => {
      pet = age(ageVal)(initialState)
    })

    it('should have added age', () => {
      expect(prop('age', pet)).to.eql(ageVal)
    })

    it('should set stage', () => {
      expect(prop('stage', pet)).to.eql('Baby')
    })
  })
})