"use strict";
import chai from "chai";
import { expect } from "chai";
import sinon from "sinon";
import sinonChai from "sinon-chai";
import { prop } from 'ramda';

import initialState from '../src/constants/initial-state'
import setState from '../src/mutations/set-state'

chai.should();
chai.use(sinonChai);

describe('Set State', () => {
  let pet, state

  beforeEach('set pet', () => {
    pet = initialState
  })

  beforeEach('set value', () => {
    state = 'Asleep'
  })

  context('when set state', () => {
    beforeEach('set state', () => {
      pet = setState(state)('state')(pet)
    })

    it('should have set state', () => {
      expect(prop('state', pet)).to.eql(state)
    })
  })
})