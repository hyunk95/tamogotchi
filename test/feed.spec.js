"use strict";
import chai from "chai";
import { expect } from "chai";
import sinon from "sinon";
import sinonChai from "sinon-chai";
import { prop } from 'ramda';
import initialState from '../src/constants/initial-state'

import feed from '../src/mutations/feed'

chai.should();
chai.use(sinonChai);

describe('Feed', () => {
  let oldPet, newPet, value

  beforeEach('set pet', () => {
    oldPet = initialState
  })

  beforeEach('set value', () => {
    value = { name: 'lunch', value: 20 }
  })

  context('when fed lunch', () => {
    beforeEach('feed pet', () => {
      newPet = feed(prop('name', value))(initialState)
    })

    it('should have increased by the value', () => {
      expect(
        prop('fullness', newPet) - prop('fullness', oldPet)
      ).to.eql(
        prop('value', value)
      )
    })
  })
})