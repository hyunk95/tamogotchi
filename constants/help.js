import colors from 'colors'

const help = [ colors.grey('.actions        ' + 'possible actions')
             ,colors.grey('.status       ' + 'status of your pet')
             ,colors.grey('.q[uit]      ' + 'exit console.')
             ].join('\n')