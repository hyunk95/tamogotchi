import { path } from 'ramda';

import feed from '../mutations/feed';
import checkPet from '../mutations/check-pet';
import age from '../mutations/age';
import setState from '../mutations/set-state';

export default req => ({
  feed: { data: path(['body','foodChoice'], req), fn: feed },
  age: { data: path(['body','time'], req), fn: age },
  sleep: { fn: setState('Asleep')('state') },
  checkPet: { data: path(['body','seed'], req), fn: checkPet }
})