export default {
  health: 100,
  fullness: 100,
  age: 0,
  stage: 'Egg',
  state: 'Asleep',
  stoolCounter: 0
}