import State from '../types/state';
import { compose, map, when } from 'ramda'

const get = State.get;

export default predicate => fn =>
  compose(
    map(
      when(
        predicate,
        fn
      )
    ),
  )(get)