// isCharacterInAlphabet :: String -> Bool
export default str =>
  str.length === 1 && str.match(/[a-z]/i)