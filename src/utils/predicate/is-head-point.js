import { compose, equals, head } from 'ramda'

// isHeadPoint :: String -> Bool
module.exports =
  compose(
    equals('.'),
    head
  )