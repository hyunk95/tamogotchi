import {
  reduce,
  compose, 
  converge,
  path,
  prop
} from 'ramda'

import methods from '../constants/methods';

export default arr => req =>
  reduce((state, fnName) =>
    compose(
      converge(
        (data, fn) =>
          data && fn(data)(state) || fn(state)
        ,
        [
          prop('data'),
          prop('fn')
        ]
      ),
      prop(fnName),
      methods
    )(req)
  , path(['body', 'state'], req), arr)