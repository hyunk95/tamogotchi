import { compose, flip, invoker, path } from 'ramda'

export default (req, res) => fn =>
  compose(
    flip(invoker(1, 'send'))(res),
    fn
  )(req)