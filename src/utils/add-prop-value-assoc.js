import { compose, assoc, reduce, ap, prop, add } from 'ramda';

export default propName => val => s =>
  compose(
    assoc(
      propName,
      compose(
        reduce(add, 0),
        ap([prop(propName)])
      )([val, s])
    )
  )(s)