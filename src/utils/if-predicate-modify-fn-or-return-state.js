import { 
  ifElse,
  always,
  curry
} from 'ramda'

import State from '../types/state'

const of = State.of;
const modify = State.modify;

export default predicate => fn =>
  ifElse(
    predicate,
    () => modify(
      always(
        fn
      )
    ),
    curry(of)
  )