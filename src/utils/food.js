const FOOD = {
  'snack': 10,
  'lunch': 20,
  'dinner': 30
}

export default choice =>
  FOOD[choice] || 0

export {
  FOOD
}