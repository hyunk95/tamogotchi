import express from 'express';
import bodyParser from 'body-parser';

import getReqFnResSend from './utils/get-req-fn-res-send';
import reduceArrayMethods from './utils/reduce-array-methods';

const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))

app.post('/feed', (req,res) =>
  getReqFnResSend(req,res)(reduceArrayMethods(['feed', 'age', 'checkPet']))
)

app.post('/sleep', (req, res) =>
  getReqFnResSend(req,res)(reduceArrayMethods(['age', 'sleep']))
)

app.post('/check_status', (req, res) => {
  getReqFnResSend(req,res)(reduceArrayMethods(['age', 'checkPet']))
})

if(!module.parent){ app.listen(port, () => 
  console.log(`Listening on port ${port}`)
) }

module.exports = app;