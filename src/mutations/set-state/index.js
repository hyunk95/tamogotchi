import { compose, chain } from 'ramda';

import initialState from '../../constants/initial-state';
import updateState from './helpers/update-state';
import State from '../../types/state';

const setState = prop =>
  compose(
    chain(updateState(prop)),
    State.of
  )

export default state => prop => (obj = initialState) =>
  setState(prop)(state).exec(obj)