import {
  assoc
} from 'ramda';
import State from '../../../types/state';

const modify = State.modify;

export default prop => stateValue =>
  modify(assoc(prop, stateValue))
