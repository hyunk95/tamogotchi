import { compose, chain, always } from 'ramda';

import State from '../../types/state';
import ifHungryLoseHealth from './helpers/if-hungry-lose-health';
import ifDeadUpdateStage from './helpers/if-dead-update-stage';
import ifSleepyUpdateState from './helpers/if-sleepy-update-state';
import ifNeedToDefecateIncStoolCount from './helpers/if-need-to-defecate-inc-stool-count';
import pickRelevantIfDead from '../age/helpers/pick-relevant-if-dead';
import isHungry from './helpers/is-hungry';
import needToDefecate from './helpers/need-to-defecate';
import checkHealth from './helpers/check-health';
import randomSleep from './helpers/random-sleep';
import randomHunger from './helpers/random-hunger';

const checkPet = seed =>
  compose(
    chain(always(pickRelevantIfDead)),
    chain(ifDeadUpdateStage),
    chain(always(checkHealth)),
    chain(ifSleepyUpdateState),
    chain(always(randomSleep(seed))),
    chain(always(randomHunger(seed))),
    chain(ifNeedToDefecateIncStoolCount),
    chain(always(needToDefecate)),
    chain(ifHungryLoseHealth),
    chain(always(isHungry)),
    State.of
  )({})

export default seed => (obj = initialState) =>
  checkPet(seed).exec(obj)