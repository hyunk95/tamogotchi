import {
  propOr,
  evolve,
  flip,
  subtract
} from 'ramda'

import ifPredicateModifyFnOrReturnState from '../../../utils/if-predicate-modify-fn-or-return-state'

export default r =>
  ifPredicateModifyFnOrReturnState(
    propOr(false, 'isHungry')
  )(evolve({
    health: flip(subtract)(10)
  }, r))(r)