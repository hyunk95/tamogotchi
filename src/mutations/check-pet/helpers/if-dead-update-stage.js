import {
  propOr,
  assoc
} from 'ramda'

import ifPredicateModifyFnOrReturnState from '../../../utils/if-predicate-modify-fn-or-return-state'
import setState from '../../set-state'

export default r =>
  ifPredicateModifyFnOrReturnState(
    propOr(false, 'isDead')
  )(setState('Dead')('stage')(r))(r)