import {
  compose,
  map,
  assoc,
  prop
} from 'ramda';

import isOne from '../../../utils/predicate/is-one';
import { zeroToOne } from './random-seed';
import State from '../../../types/state';

const get = State.get;

export default seed =>
  compose(
    map(
      assoc(
        'isSleepy',
        compose(
          isOne,
          prop('value'),
          zeroToOne
        )(seed)
      )
    )
  )(get)