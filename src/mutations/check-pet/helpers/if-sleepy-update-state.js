import { 
  compose,
  always,
  prop,
} from 'ramda'

import State from '../../../types/state'
import setState from '../../set-state'

const of = State.of;
const modify = State.modify;

const asleepOrAwake = bool =>
  bool ? 'Asleep' : 'Awake'

export default r =>
  modify(
    always(
      compose(
        bool =>
          setState(asleepOrAwake(bool))('state')(r),
        prop('isSleepy')
      )(r)
    )
  )

export { asleepOrAwake }