import {
  propOr,
  evolve,
  compose,
  flip,
  subtract,
  add
} from 'ramda'

import ifPredicateModifyFnOrReturnState from '../../../utils/if-predicate-modify-fn-or-return-state'

export default r =>
  ifPredicateModifyFnOrReturnState(
    propOr(false, 'needToDefecate')
  )(evolve({ 
    stoolCounter: add(1),
    fullness: flip(subtract)(10)
  }, r))(r)