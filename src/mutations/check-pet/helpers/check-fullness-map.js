import {
  compose,
  gt,
  prop,
  assoc
} from 'ramda'

import whenPredicateMap from '../../../utils/when-predicate-map'

export default predicate => fn =>
  whenPredicateMap(compose(
    predicate,
    prop('fullness')
  ))(fn)