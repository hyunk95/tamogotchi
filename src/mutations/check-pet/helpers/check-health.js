import {
  compose,
  equals,
  prop,
  or,
  gt,
  converge,
  assoc
} from 'ramda';

import whenPredicateMap from '../../../utils/when-predicate-map';

export default
  whenPredicateMap(compose(
    converge(
      or,
      [
        compose(
          equals(0),
          prop('health')
        ),
        compose(
          gt(0),
          prop('fullness')
        )
      ]
    )
  ))(
    assoc('isDead', true)
  )