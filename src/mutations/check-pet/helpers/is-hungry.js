import {
  gt,
  assoc
} from 'ramda'

import checkFullnessMap from './check-fullness-map'

export default checkFullnessMap(gt(30))(assoc('isHungry', true))