import {
  evolve,
  flip,
  subtract,
} from 'ramda';

import { zeroToTen } from './random-seed';
import State from '../../../types/state';

const modify = State.modify;

export default seed =>
  modify(
    evolve({
      fullness: flip(subtract)(zeroToTen(seed).value)
    })
  )
