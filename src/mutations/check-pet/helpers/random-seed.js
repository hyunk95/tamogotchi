import rnd from '../../../utils/random'

const zeroToOne = seed =>
  rnd.random(seed, 0, 1)

const zeroToTen = seed =>
  rnd.random(seed, 0, 10)

export {
  zeroToOne,
  zeroToTen
}