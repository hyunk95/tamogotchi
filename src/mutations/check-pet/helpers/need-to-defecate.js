import {
  lt,
  assoc
} from 'ramda'

import checkFullnessMap from './check-fullness-map'

export default
  checkFullnessMap(lt(70))(assoc('needToDefecate', true))