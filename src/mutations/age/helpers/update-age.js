import State from '../../../types/state';
import { assoc } from 'ramda';

const modify = State.modify;

export default time =>
  modify(assoc('age', time))