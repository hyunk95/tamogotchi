import { compose, when, equals, prop, pick } from 'ramda'

import State from '../../../types/state';

const modify = State.modify;

export default
  modify(
    compose(
      when(
        compose(
          equals('Death'),
          prop('stage')
        ),
        pick(['stage'])
      )
    )
  )