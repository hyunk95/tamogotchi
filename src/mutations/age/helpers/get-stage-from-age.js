import { compose, prop, find, ap, gt, lt, of, apply, __, and } from 'ramda'
import AGES from '../../../constants/ages'

export default s =>
  compose(
    prop('stage'),
    find(
      compose(
        apply(and),
        ap([
          compose( 
            gt(prop('age', s)),
            prop('min')
          ), 
          compose(
            lt(prop('age', s)),
            prop('max')
          )
        ]),
        of
      )
    ),
  )(AGES)