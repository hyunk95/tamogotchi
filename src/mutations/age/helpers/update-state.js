import {
  compose, 
  apply,
  prepend,
  __,
  uncurryN
} from 'ramda'

import State from '../../../types/state';
import setState from '../../set-state';
import getStageFromAge from './get-stage-from-age';

const modify = State.modify;

const uncurriedSetState =
  uncurryN(3, setState)

export default
  modify(
    s =>
      compose(
        apply(uncurriedSetState),
        prepend(__, ['stage', s]),
        getStageFromAge
      )(s)
  )