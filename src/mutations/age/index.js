import { chain, objOf, always, __, compose } from 'ramda';

import State from '../../types/state';
import initialState from '../../constants/initial-state';
import pickRelevantIfDead from './helpers/pick-relevant-if-dead';
import updateState from './helpers/update-state';
import updateAge from './helpers/update-age';

const age =
  compose(
    chain(always(pickRelevantIfDead)),
    chain(always(updateState)),
    chain(updateAge),
    State.of
  )

export default time => (obj = initialState) =>
  age(time).exec(obj)