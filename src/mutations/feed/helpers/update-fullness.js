import State from '../../../types/state';
import increaseFullness from './increase-fullness';

const modify = State.modify;

export default foodValue =>
  modify(increaseFullness(foodValue))
