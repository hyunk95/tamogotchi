import { compose, chain, objOf } from 'ramda';

import State from '../../types/state';
import initialState from '../../constants/initial-state';
import food from '../../utils/food';
import updateFullness from './helpers/update-fullness';

const feedPet =
  compose(
    chain(updateFullness),
    State.of,
    objOf('fullness'),
    food
  )

export default choice => (obj = initialState) =>
  feedPet(choice).exec(obj)