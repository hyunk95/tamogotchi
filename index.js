import readline from 'readline';
import colors from 'colors';

import completer from './helpers/completer';
import initialState from './src/constants/initial-state';
import seed from './src/utils/random';
import getKeysAndValues from './helpers/get-keys-and-values';
import isHeadPoint from './src/utils/predicate/is-head-point';
import isCharacterInAlphabet from './src/utils/predicate/is-character-in-alphabet';
import { CronJob } from 'cron';
import actionsString from './helpers/actions-string';
import help from './constants/help';
import diffString from './helpers/diff-string';
import actionsWithPostCalls from './helpers/actions-with-post-calls';
import foodChoices from './helpers/food-choices';

var pet = initialState;

const rl = readline.createInterface(
  process.stdin, 
  process.stdout, 
  completer
)

// impure function
function welcome() {
  console.log([ "TAMAGOTCHI"
            , "= Welcome, enter .help if you're lost."
            ].join('\n').grey);

  prompt();
}

// impure function
function prompt() {
  var arrow    = '> '
    , length = arrow.length
    ;

  rl.setPrompt(colors.grey(arrow), length);
  rl.prompt();
}

new CronJob('0,10 * * * * *', function() {
  actionsWithPostCalls('check_status')({
    state: pet, 
    seed: seed.genCsSeed(), 
    time: process.uptime()
  })(
    state => {
      console.log(diffString(pet)(state))
      pet = state
    }
  )
}, null, true, 'America/Los_Angeles');

// impure function
function exec(command) {
  let int = parseInt(command)

  if(isHeadPoint(command)) {
    let action = command.slice(1)
    switch(action) {
      case 'help':
        console.log(help.yellow);
        break;
      case 'actions':
        console.log(actionsString.yellow);
        break;
      case 'status':
        console.log(pet)
        break;
      case 'exit':
        break;
      case 'q':
        process.exit(0);
        break;
    }
  }

  if(!isNaN(int)) {
    let val = foodChoices[int]

    !!val
      ? actionsWithPostCalls('feed')({
          state: pet, 
          seed: seed.genCsSeed(), 
          time: process.uptime(),
          foodChoice: val
        })(
          state => {
            pet = state;
            console.log(`Your pet was fed!`)
            prompt();
          }
        )
      : console.log('Invalid Food Choice')
  }

  if(isCharacterInAlphabet(command)) {
    switch(command) {
      case 'a':
        actionsWithPostCalls('sleep')({
          state: pet, 
          seed: seed.genCsSeed(), 
          time: process.uptime()
        })(
          state => {
            pet = state;
            console.log(`Your pet is now asleep`)
            prompt();
          }
        )
        break;
      case 'b':
        console.log('What would you like to feed it?')
        console.log(getKeysAndValues(foodChoices))
        break;
    }
  }
  
  prompt();
}

rl.on('line', function(cmd) {
  exec(cmd.trim());
}).on('close', function() {
  console.log('goodbye!'.green);
  process.exit(0);
});

welcome();