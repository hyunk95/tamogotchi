import { keys, zipObj, compose } from 'ramda';
import { FOOD } from '../src/utils/food';

export default
  compose(
    zipObj([1,2,3]),
    keys
  )(FOOD)