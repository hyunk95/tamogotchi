import { compose, mapObjIndexed, join, values } from 'ramda'; 
import diff from '../src/utils/diff-obj';

const keyRelevantString = ([key, data]) => ({
  'health': `Health is now ${data}`,
  'fullness': `Fullness is now ${data}`,
  'age': `Age is now ${data}`,
  'stage': `Congratulations your pet is now ${data}`,
  'state': `Your pet is ${data}`,
  'stoolCounter': `Ewww your pet pooped`,
  'isSleepy': 'Your pet is starting to get sleepy',
  'isHungry': 'YOUR PET IS HUNGRY FEED HIM/HER'
})

export default previousState =>
  compose(
    join('\n'),
    values,
    mapObjIndexed(
      (num, key, obj) =>
        keyRelevantString(obj[key])[key]
    ),
    diff(previousState)
  )