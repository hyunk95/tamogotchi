import {
  compose,
  join,
  values,
  mapObjIndexed
} from 'ramda'

export default
  compose(
    join('\n'),
    values,
    mapObjIndexed(
      (num, key, obj) =>
      `${key}: ${obj[key]}`,
    ),
  )
