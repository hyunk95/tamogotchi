import { compose, join, map }  from 'ramda'
import actions from '../constants/actions'

export default
  compose(
    join('\n'),
    map(
      action =>
        `[${action.key}] - ${action.action}`
    )
  )(actions)