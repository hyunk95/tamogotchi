import futureRequest from './future-request';

export default route => data => cb =>
  futureRequest({
    url: `http://localhost:3000/${route}`,
    method: "POST",
    json: data
  })
  .fork(console.error, cb)