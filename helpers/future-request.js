import Future from 'fluture';
import request from 'request';

export default data =>
  Future((rej, res) => {
    request(data, (err, response, body) => {
      err 
        ? rej(err) 
        : res(response.body)
    })
  })