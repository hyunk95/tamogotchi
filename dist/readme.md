# Tamagotchi

## Installation

`yarn install`

## Running the app

```
yarn server
yarn start

yarn test - for tests
```

## Guide 

```
.help - to see the menu
.status - to see the raw state of the pet
.actions - to see the possible actions
```
  
## API endpoints

Traditionally, endpoints should contain an id of the object and a verb - however for this implementation, it contains just a verb as there are no implementations of models within the application.

The endpoints that cause a mutation have some level of randomness to change the state of the pet. To keep the mutations pure, we pass a seed file.

Every time the endpoint is hit, it will age the pet according to how the long the server was running.

## Feed

This endpoint will feed the pet and increase the fullness.

```
endpoint: /feed
query: {
	@state: {
		health: Int!,
		fullness: Int!,
		age: Int!,
		stage: String!,
		state: String!,
		stoolCounter: 0!,
		isSleepy: boolean,
		needToDefecate: boolean,
		isHungry: boolean,
	},
	@seed: https://github.com/Risto-Stevcev/pure-random,
	@time: Float!,
	@foodChoice: String!
}

output: @state
```

## Sleep

This endpoint will make the pet sleep.

```
endpoint: /sleep
query: {
	@state: {
		health: Int!,
		fullness: Int!,
		age: Int!,
		stage: String!,
		state: String!,
		stoolCounter: 0!,
		isSleepy: boolean,
		needToDefecate: boolean,
		isHungry: boolean,
	},
	@time: Float!,
}

output: @state
```

## Check Status

This endpoint will check the status of the pet and mutate the state according to some level of randomness.  The following can occur:
*  lose health due to hunger
*  increase stool count
*  lose fullness
*  fall asleep / awaken
*  if health equal to 0 or fullness is less than 0, change the state of the pet to "Dead"

```
endpoint: /sleep
query: {
	@state: {
		health: Int!,
		fullness: Int!,
		age: Int!,
		stage: String!,
		state: String!,
		stoolCounter: 0!,
		isSleepy: boolean,
		needToDefecate: boolean,
		isHungry: boolean,
	},
	@time: Float!,
}

output: @state
```