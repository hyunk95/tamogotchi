"use strict";

var _chai = require("chai");

var _chai2 = _interopRequireDefault(_chai);

var _sinon = require("sinon");

var _sinon2 = _interopRequireDefault(_sinon);

var _sinonChai = require("sinon-chai");

var _sinonChai2 = _interopRequireDefault(_sinonChai);

var _ramda = require("ramda");

var _randomSeed = require("../src/mutations/check-pet/helpers/random-seed");

var _isOne = require("../src/utils/predicate/is-one");

var _isOne2 = _interopRequireDefault(_isOne);

var _initialState = require("../src/constants/initial-state");

var _initialState2 = _interopRequireDefault(_initialState);

var _checkPet = require("../src/mutations/check-pet");

var _checkPet2 = _interopRequireDefault(_checkPet);

var _random = require("../src/utils/random");

var _random2 = _interopRequireDefault(_random);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_chai2.default.should();
_chai2.default.use(_sinonChai2.default);

describe('Check Pet', function () {
  var pet = void 0,
      newPet = void 0,
      seed = void 0;

  beforeEach('set pet', function () {
    pet = (0, _ramda.assoc)('fullness', 10, _initialState2.default);
  });

  beforeEach('set seed', function () {
    seed = _random2.default.genCsSeed();
  });

  context('when checking Pet', function () {
    context('when pet is alive', function () {
      context('when hungry', function () {

        beforeEach('checkPet', function () {
          newPet = (0, _checkPet2.default)(seed)(pet);
        });

        it('should have added isHungry flag', function () {
          (0, _chai.expect)((0, _ramda.prop)('isHungry', newPet)).to.eql(true);
        });

        it('should have decreased health', function () {
          (0, _chai.expect)((0, _ramda.prop)('health', newPet)).to.eql((0, _ramda.prop)('health', pet) - 10);
        });
      });

      context('when need to defecate', function () {
        beforeEach('set fullness', function () {
          newPet = (0, _ramda.assoc)('fullness', 80, _initialState2.default);
        });

        beforeEach('checkPet', function () {
          newPet = (0, _checkPet2.default)(seed)(newPet);
        });

        it('should have incremented stoolCount', function () {
          (0, _chai.expect)((0, _ramda.prop)('stoolCounter', newPet)).to.eql(1);
        });
      });

      context('when not sleepy', function () {
        beforeEach('checkPet', function () {
          newPet = (0, _checkPet2.default)(seed)(pet);
        });

        it('should be sleepy', function () {
          (0, _chai.expect)((0, _ramda.propOr)(false, 'isSleepy', newPet)).to.eql((0, _ramda.compose)(_isOne2.default, (0, _ramda.prop)('value'), _randomSeed.zeroToOne)(seed));
        });
      });
    });

    context('when pet is dead', function () {
      beforeEach('set pet', function () {
        pet = (0, _ramda.assoc)('health', 10, pet);
      });

      beforeEach('checkPet', function () {
        pet = (0, _checkPet2.default)(seed)(pet);
      });

      it('should have stage as dead', function () {
        (0, _chai.expect)((0, _ramda.prop)('stage', pet)).to.eql('Dead');
      });
    });
  });
});