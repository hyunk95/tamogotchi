"use strict";

var _chai = require("chai");

var _chai2 = _interopRequireDefault(_chai);

var _sinon = require("sinon");

var _sinon2 = _interopRequireDefault(_sinon);

var _sinonChai = require("sinon-chai");

var _sinonChai2 = _interopRequireDefault(_sinonChai);

var _ramda = require("ramda");

var _initialState = require("../src/constants/initial-state");

var _initialState2 = _interopRequireDefault(_initialState);

var _age = require("../src/mutations/age");

var _age2 = _interopRequireDefault(_age);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_chai2.default.should();
_chai2.default.use(_sinonChai2.default);

describe('Age', function () {
  var pet = void 0,
      ageVal = void 0;

  beforeEach('set pet', function () {
    pet = _initialState2.default;
  });

  beforeEach('set value', function () {
    ageVal = 100;
  });

  context('when adding age', function () {
    beforeEach('add age', function () {
      pet = (0, _age2.default)(ageVal)(_initialState2.default);
    });

    it('should have added age', function () {
      (0, _chai.expect)((0, _ramda.prop)('age', pet)).to.eql(ageVal);
    });

    it('should set stage', function () {
      (0, _chai.expect)((0, _ramda.prop)('stage', pet)).to.eql('Baby');
    });
  });
});