"use strict";

var _chai = require("chai");

var _chai2 = _interopRequireDefault(_chai);

var _sinon = require("sinon");

var _sinon2 = _interopRequireDefault(_sinon);

var _sinonChai = require("sinon-chai");

var _sinonChai2 = _interopRequireDefault(_sinonChai);

var _ramda = require("ramda");

var _initialState = require("../src/constants/initial-state");

var _initialState2 = _interopRequireDefault(_initialState);

var _setState = require("../src/mutations/set-state");

var _setState2 = _interopRequireDefault(_setState);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_chai2.default.should();
_chai2.default.use(_sinonChai2.default);

describe('Set State', function () {
  var pet = void 0,
      state = void 0;

  beforeEach('set pet', function () {
    pet = _initialState2.default;
  });

  beforeEach('set value', function () {
    state = 'Asleep';
  });

  context('when set state', function () {
    beforeEach('set state', function () {
      pet = (0, _setState2.default)(state)('state')(pet);
    });

    it('should have set state', function () {
      (0, _chai.expect)((0, _ramda.prop)('state', pet)).to.eql(state);
    });
  });
});