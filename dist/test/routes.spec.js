"use strict";

var _chai = require("chai");

var _chai2 = _interopRequireDefault(_chai);

var _sinon = require("sinon");

var _sinon2 = _interopRequireDefault(_sinon);

var _sinonChai = require("sinon-chai");

var _sinonChai2 = _interopRequireDefault(_sinonChai);

var _supertest = require("supertest");

var _supertest2 = _interopRequireDefault(_supertest);

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _ramda = require("ramda");

var _initialState = require("../src/constants/initial-state");

var _initialState2 = _interopRequireDefault(_initialState);

var _random = require("../src/utils/random");

var _random2 = _interopRequireDefault(_random);

var _randomSeed = require("../src/mutations/check-pet/helpers/random-seed");

var _isOne = require("../src/utils/predicate/is-one");

var _isOne2 = _interopRequireDefault(_isOne);

var _ifSleepyUpdateState = require("../src/mutations/check-pet/helpers/if-sleepy-update-state");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe('Routes', function () {
  var params = void 0,
      app = void 0,
      seed = void 0,
      boolean = void 0;

  beforeEach('import app', function () {
    app = require('../src/index');
  });

  describe('Feed', function () {
    beforeEach('set seed', function () {
      seed = _random2.default.genCsSeed();
      boolean = (0, _ramda.compose)(_isOne2.default, (0, _ramda.prop)('value'), _randomSeed.zeroToOne)(seed);
    });

    beforeEach('set params', function () {
      params = {
        state: _initialState2.default,
        seed: seed,
        time: 100,
        foodChoice: 'snack'
      };
    });

    it('should return res', function (done) {
      (0, _supertest2.default)(app.listen()).post('/feed').send(params).set('Accept', 'application/json').expect(200).then(function (response) {
        (0, _chai.expect)(response.body).to.eql({
          health: 100,
          fullness: 100 - (0, _randomSeed.zeroToTen)(seed).value,
          age: 100,
          stage: 'Baby',
          state: (0, _ifSleepyUpdateState.asleepOrAwake)(boolean),
          stoolCounter: 1,
          needToDefecate: true,
          isSleepy: boolean
        });
      }).finally(function () {
        done();
      });
    });
  });

  describe('Sleep', function () {
    beforeEach('set params', function () {
      params = {
        state: (0, _ramda.assoc)('state', 'Awake', _initialState2.default),
        time: 100
      };
    });

    it('should return res', function (done) {
      (0, _supertest2.default)(app.listen()).post('/sleep').send(params).set('Accept', 'application/json').expect(200).then(function (response) {
        (0, _chai.expect)(response.body).to.eql({
          health: 100,
          fullness: 100,
          age: 100,
          stage: 'Baby',
          state: 'Asleep',
          stoolCounter: 0 });
      }).finally(function () {
        done();
      });
    });
  });

  describe('Check Status', function () {
    beforeEach('set seed', function () {
      seed = _random2.default.genCsSeed();
      boolean = (0, _ramda.compose)(_isOne2.default, (0, _ramda.prop)('value'), _randomSeed.zeroToOne)(seed);
    });

    beforeEach('set params', function () {
      params = {
        state: _initialState2.default,
        seed: seed,
        time: 2000
      };
    });

    it('should return res', function (done) {
      (0, _supertest2.default)(app.listen()).post('/check_status').send(params).set('Accept', 'application/json').expect(200).then(function (response) {
        (0, _chai.expect)(response.body).to.eql({
          stage: 'Death'
        });
      }).finally(function () {
        done();
      });
    });
  });
});