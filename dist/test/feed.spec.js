"use strict";

var _chai = require("chai");

var _chai2 = _interopRequireDefault(_chai);

var _sinon = require("sinon");

var _sinon2 = _interopRequireDefault(_sinon);

var _sinonChai = require("sinon-chai");

var _sinonChai2 = _interopRequireDefault(_sinonChai);

var _ramda = require("ramda");

var _initialState = require("../src/constants/initial-state");

var _initialState2 = _interopRequireDefault(_initialState);

var _feed = require("../src/mutations/feed");

var _feed2 = _interopRequireDefault(_feed);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_chai2.default.should();
_chai2.default.use(_sinonChai2.default);

describe('Feed', function () {
  var oldPet = void 0,
      newPet = void 0,
      value = void 0;

  beforeEach('set pet', function () {
    oldPet = _initialState2.default;
  });

  beforeEach('set value', function () {
    value = { name: 'lunch', value: 20 };
  });

  context('when fed lunch', function () {
    beforeEach('feed pet', function () {
      newPet = (0, _feed2.default)((0, _ramda.prop)('name', value))(_initialState2.default);
    });

    it('should have increased by the value', function () {
      (0, _chai.expect)((0, _ramda.prop)('fullness', newPet) - (0, _ramda.prop)('fullness', oldPet)).to.eql((0, _ramda.prop)('value', value));
    });
  });
});