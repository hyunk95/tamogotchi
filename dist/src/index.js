'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _getReqFnResSend = require('./utils/get-req-fn-res-send');

var _getReqFnResSend2 = _interopRequireDefault(_getReqFnResSend);

var _reduceArrayMethods = require('./utils/reduce-array-methods');

var _reduceArrayMethods2 = _interopRequireDefault(_reduceArrayMethods);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = (0, _express2.default)();
var port = 3000;

app.use(_bodyParser2.default.json());
app.use(_bodyParser2.default.urlencoded({ extended: true }));

app.post('/feed', function (req, res) {
  return (0, _getReqFnResSend2.default)(req, res)((0, _reduceArrayMethods2.default)(['feed', 'age', 'checkPet']));
});

app.post('/sleep', function (req, res) {
  return (0, _getReqFnResSend2.default)(req, res)((0, _reduceArrayMethods2.default)(['age', 'sleep']));
});

app.post('/check_status', function (req, res) {
  (0, _getReqFnResSend2.default)(req, res)((0, _reduceArrayMethods2.default)(['age', 'checkPet']));
});

if (!module.parent) {
  app.listen(port, function () {
    return console.log('Listening on port ' + port);
  });
}

module.exports = app;