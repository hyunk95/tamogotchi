'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = [{
  stage: 'Egg', min: 0, max: 60
}, {
  stage: 'Baby', min: 60, max: 120
}, {
  stage: 'Child', min: 120, max: 180
}, {
  stage: 'Teenager', min: 180, max: 360
}, {
  stage: 'Adult', min: 360, max: 720
}, {
  stage: 'Senior', min: 720, max: 1080
}, {
  stage: 'Death', min: 1080, max: Infinity
}];