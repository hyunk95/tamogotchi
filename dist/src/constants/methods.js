'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ramda = require('ramda');

var _feed = require('../mutations/feed');

var _feed2 = _interopRequireDefault(_feed);

var _checkPet = require('../mutations/check-pet');

var _checkPet2 = _interopRequireDefault(_checkPet);

var _age = require('../mutations/age');

var _age2 = _interopRequireDefault(_age);

var _setState = require('../mutations/set-state');

var _setState2 = _interopRequireDefault(_setState);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (req) {
  return {
    feed: { data: (0, _ramda.path)(['body', 'foodChoice'], req), fn: _feed2.default },
    age: { data: (0, _ramda.path)(['body', 'time'], req), fn: _age2.default },
    sleep: { fn: (0, _setState2.default)('Asleep')('state') },
    checkPet: { data: (0, _ramda.path)(['body', 'seed'], req), fn: _checkPet2.default }
  };
};