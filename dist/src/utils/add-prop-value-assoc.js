'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ramda = require('ramda');

exports.default = function (propName) {
  return function (val) {
    return function (s) {
      return (0, _ramda.compose)((0, _ramda.assoc)(propName, (0, _ramda.compose)((0, _ramda.reduce)(_ramda.add, 0), (0, _ramda.ap)([(0, _ramda.prop)(propName)]))([val, s])))(s);
    };
  };
};