'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ramda = require('ramda');

exports.default = function (req, res) {
  return function (fn) {
    return (0, _ramda.compose)((0, _ramda.flip)((0, _ramda.invoker)(1, 'send'))(res), fn)(req);
  };
};