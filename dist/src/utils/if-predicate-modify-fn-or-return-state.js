'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ramda = require('ramda');

var _state = require('../types/state');

var _state2 = _interopRequireDefault(_state);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var of = _state2.default.of;
var modify = _state2.default.modify;

exports.default = function (predicate) {
  return function (fn) {
    return (0, _ramda.ifElse)(predicate, function () {
      return modify((0, _ramda.always)(fn));
    }, (0, _ramda.curry)(of));
  };
};