'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var FOOD = {
  'snack': 10,
  'lunch': 20,
  'dinner': 30
};

exports.default = function (choice) {
  return FOOD[choice] || 0;
};

exports.FOOD = FOOD;