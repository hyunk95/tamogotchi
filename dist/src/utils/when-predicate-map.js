'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _state = require('../types/state');

var _state2 = _interopRequireDefault(_state);

var _ramda = require('ramda');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var get = _state2.default.get;

exports.default = function (predicate) {
  return function (fn) {
    return (0, _ramda.compose)((0, _ramda.map)((0, _ramda.when)(predicate, fn)))(get);
  };
};