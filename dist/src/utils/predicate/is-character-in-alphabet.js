"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

// isCharacterInAlphabet :: String -> Bool
exports.default = function (str) {
  return str.length === 1 && str.match(/[a-z]/i);
};