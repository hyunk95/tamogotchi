'use strict';

var _ramda = require('ramda');

// isHeadPoint :: String -> Bool
module.exports = (0, _ramda.compose)((0, _ramda.equals)('.'), _ramda.head);