'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ramda = require('ramda');

var _methods = require('../constants/methods');

var _methods2 = _interopRequireDefault(_methods);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (arr) {
  return function (req) {
    return (0, _ramda.reduce)(function (state, fnName) {
      return (0, _ramda.compose)((0, _ramda.converge)(function (data, fn) {
        return data && fn(data)(state) || fn(state);
      }, [(0, _ramda.prop)('data'), (0, _ramda.prop)('fn')]), (0, _ramda.prop)(fnName), _methods2.default)(req);
    }, (0, _ramda.path)(['body', 'state'], req), arr);
  };
};