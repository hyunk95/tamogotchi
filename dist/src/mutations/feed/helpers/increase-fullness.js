'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _addPropValueAssoc = require('../../../utils/add-prop-value-assoc');

var _addPropValueAssoc2 = _interopRequireDefault(_addPropValueAssoc);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = (0, _addPropValueAssoc2.default)('fullness');