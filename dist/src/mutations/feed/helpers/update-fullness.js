'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _state = require('../../../types/state');

var _state2 = _interopRequireDefault(_state);

var _increaseFullness = require('./increase-fullness');

var _increaseFullness2 = _interopRequireDefault(_increaseFullness);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var modify = _state2.default.modify;

exports.default = function (foodValue) {
  return modify((0, _increaseFullness2.default)(foodValue));
};