'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ramda = require('ramda');

var _state = require('../../types/state');

var _state2 = _interopRequireDefault(_state);

var _initialState = require('../../constants/initial-state');

var _initialState2 = _interopRequireDefault(_initialState);

var _food = require('../../utils/food');

var _food2 = _interopRequireDefault(_food);

var _updateFullness = require('./helpers/update-fullness');

var _updateFullness2 = _interopRequireDefault(_updateFullness);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var feedPet = (0, _ramda.compose)((0, _ramda.chain)(_updateFullness2.default), _state2.default.of, (0, _ramda.objOf)('fullness'), _food2.default);

exports.default = function (choice) {
  return function () {
    var obj = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _initialState2.default;
    return feedPet(choice).exec(obj);
  };
};