'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.asleepOrAwake = undefined;

var _ramda = require('ramda');

var _state = require('../../../types/state');

var _state2 = _interopRequireDefault(_state);

var _setState = require('../../set-state');

var _setState2 = _interopRequireDefault(_setState);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var of = _state2.default.of;
var modify = _state2.default.modify;

var asleepOrAwake = function asleepOrAwake(bool) {
  return bool ? 'Asleep' : 'Awake';
};

exports.default = function (r) {
  return modify((0, _ramda.always)((0, _ramda.compose)(function (bool) {
    return (0, _setState2.default)(asleepOrAwake(bool))('state')(r);
  }, (0, _ramda.prop)('isSleepy'))(r)));
};

exports.asleepOrAwake = asleepOrAwake;