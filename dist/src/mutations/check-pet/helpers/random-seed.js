'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.zeroToTen = exports.zeroToOne = undefined;

var _random = require('../../../utils/random');

var _random2 = _interopRequireDefault(_random);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var zeroToOne = function zeroToOne(seed) {
  return _random2.default.random(seed, 0, 1);
};

var zeroToTen = function zeroToTen(seed) {
  return _random2.default.random(seed, 0, 10);
};

exports.zeroToOne = zeroToOne;
exports.zeroToTen = zeroToTen;