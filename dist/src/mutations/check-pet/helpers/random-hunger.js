'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ramda = require('ramda');

var _randomSeed = require('./random-seed');

var _state = require('../../../types/state');

var _state2 = _interopRequireDefault(_state);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var modify = _state2.default.modify;

exports.default = function (seed) {
  return modify((0, _ramda.evolve)({
    fullness: (0, _ramda.flip)(_ramda.subtract)((0, _randomSeed.zeroToTen)(seed).value)
  }));
};