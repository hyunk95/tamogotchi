'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ramda = require('ramda');

var _ifPredicateModifyFnOrReturnState = require('../../../utils/if-predicate-modify-fn-or-return-state');

var _ifPredicateModifyFnOrReturnState2 = _interopRequireDefault(_ifPredicateModifyFnOrReturnState);

var _setState = require('../../set-state');

var _setState2 = _interopRequireDefault(_setState);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (r) {
  return (0, _ifPredicateModifyFnOrReturnState2.default)((0, _ramda.propOr)(false, 'isDead'))((0, _setState2.default)('Dead')('stage')(r))(r);
};