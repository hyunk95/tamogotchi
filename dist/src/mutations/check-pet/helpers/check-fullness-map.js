'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ramda = require('ramda');

var _whenPredicateMap = require('../../../utils/when-predicate-map');

var _whenPredicateMap2 = _interopRequireDefault(_whenPredicateMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (predicate) {
  return function (fn) {
    return (0, _whenPredicateMap2.default)((0, _ramda.compose)(predicate, (0, _ramda.prop)('fullness')))(fn);
  };
};