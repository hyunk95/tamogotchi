'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ramda = require('ramda');

var _whenPredicateMap = require('../../../utils/when-predicate-map');

var _whenPredicateMap2 = _interopRequireDefault(_whenPredicateMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = (0, _whenPredicateMap2.default)((0, _ramda.compose)((0, _ramda.converge)(_ramda.or, [(0, _ramda.compose)((0, _ramda.equals)(0), (0, _ramda.prop)('health')), (0, _ramda.compose)((0, _ramda.gt)(0), (0, _ramda.prop)('fullness'))])))((0, _ramda.assoc)('isDead', true));