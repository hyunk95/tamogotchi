'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ramda = require('ramda');

var _ifPredicateModifyFnOrReturnState = require('../../../utils/if-predicate-modify-fn-or-return-state');

var _ifPredicateModifyFnOrReturnState2 = _interopRequireDefault(_ifPredicateModifyFnOrReturnState);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (r) {
  return (0, _ifPredicateModifyFnOrReturnState2.default)((0, _ramda.propOr)(false, 'needToDefecate'))((0, _ramda.evolve)({
    stoolCounter: (0, _ramda.add)(1),
    fullness: (0, _ramda.flip)(_ramda.subtract)(10)
  }, r))(r);
};