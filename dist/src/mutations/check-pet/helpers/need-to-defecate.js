'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ramda = require('ramda');

var _checkFullnessMap = require('./check-fullness-map');

var _checkFullnessMap2 = _interopRequireDefault(_checkFullnessMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = (0, _checkFullnessMap2.default)((0, _ramda.lt)(70))((0, _ramda.assoc)('needToDefecate', true));