'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ramda = require('ramda');

var _isOne = require('../../../utils/predicate/is-one');

var _isOne2 = _interopRequireDefault(_isOne);

var _randomSeed = require('./random-seed');

var _state = require('../../../types/state');

var _state2 = _interopRequireDefault(_state);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var get = _state2.default.get;

exports.default = function (seed) {
  return (0, _ramda.compose)((0, _ramda.map)((0, _ramda.assoc)('isSleepy', (0, _ramda.compose)(_isOne2.default, (0, _ramda.prop)('value'), _randomSeed.zeroToOne)(seed))))(get);
};