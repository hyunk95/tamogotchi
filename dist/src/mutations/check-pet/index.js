'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ramda = require('ramda');

var _state = require('../../types/state');

var _state2 = _interopRequireDefault(_state);

var _ifHungryLoseHealth = require('./helpers/if-hungry-lose-health');

var _ifHungryLoseHealth2 = _interopRequireDefault(_ifHungryLoseHealth);

var _ifDeadUpdateStage = require('./helpers/if-dead-update-stage');

var _ifDeadUpdateStage2 = _interopRequireDefault(_ifDeadUpdateStage);

var _ifSleepyUpdateState = require('./helpers/if-sleepy-update-state');

var _ifSleepyUpdateState2 = _interopRequireDefault(_ifSleepyUpdateState);

var _ifNeedToDefecateIncStoolCount = require('./helpers/if-need-to-defecate-inc-stool-count');

var _ifNeedToDefecateIncStoolCount2 = _interopRequireDefault(_ifNeedToDefecateIncStoolCount);

var _pickRelevantIfDead = require('../age/helpers/pick-relevant-if-dead');

var _pickRelevantIfDead2 = _interopRequireDefault(_pickRelevantIfDead);

var _isHungry = require('./helpers/is-hungry');

var _isHungry2 = _interopRequireDefault(_isHungry);

var _needToDefecate = require('./helpers/need-to-defecate');

var _needToDefecate2 = _interopRequireDefault(_needToDefecate);

var _checkHealth = require('./helpers/check-health');

var _checkHealth2 = _interopRequireDefault(_checkHealth);

var _randomSleep = require('./helpers/random-sleep');

var _randomSleep2 = _interopRequireDefault(_randomSleep);

var _randomHunger = require('./helpers/random-hunger');

var _randomHunger2 = _interopRequireDefault(_randomHunger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var checkPet = function checkPet(seed) {
  return (0, _ramda.compose)((0, _ramda.chain)((0, _ramda.always)(_pickRelevantIfDead2.default)), (0, _ramda.chain)(_ifDeadUpdateStage2.default), (0, _ramda.chain)((0, _ramda.always)(_checkHealth2.default)), (0, _ramda.chain)(_ifSleepyUpdateState2.default), (0, _ramda.chain)((0, _ramda.always)((0, _randomSleep2.default)(seed))), (0, _ramda.chain)((0, _ramda.always)((0, _randomHunger2.default)(seed))), (0, _ramda.chain)(_ifNeedToDefecateIncStoolCount2.default), (0, _ramda.chain)((0, _ramda.always)(_needToDefecate2.default)), (0, _ramda.chain)(_ifHungryLoseHealth2.default), (0, _ramda.chain)((0, _ramda.always)(_isHungry2.default)), _state2.default.of)({});
};

exports.default = function (seed) {
  return function () {
    var obj = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
    return checkPet(seed).exec(obj);
  };
};