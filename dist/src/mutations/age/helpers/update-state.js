'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ramda = require('ramda');

var _state = require('../../../types/state');

var _state2 = _interopRequireDefault(_state);

var _setState = require('../../set-state');

var _setState2 = _interopRequireDefault(_setState);

var _getStageFromAge = require('./get-stage-from-age');

var _getStageFromAge2 = _interopRequireDefault(_getStageFromAge);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var modify = _state2.default.modify;

var uncurriedSetState = (0, _ramda.uncurryN)(3, _setState2.default);

exports.default = modify(function (s) {
  return (0, _ramda.compose)((0, _ramda.apply)(uncurriedSetState), (0, _ramda.prepend)(_ramda.__, ['stage', s]), _getStageFromAge2.default)(s);
});