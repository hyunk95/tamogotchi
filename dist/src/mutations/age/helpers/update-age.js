'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _state = require('../../../types/state');

var _state2 = _interopRequireDefault(_state);

var _ramda = require('ramda');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var modify = _state2.default.modify;

exports.default = function (time) {
  return modify((0, _ramda.assoc)('age', time));
};