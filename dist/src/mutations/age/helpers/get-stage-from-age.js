'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ramda = require('ramda');

var _ages = require('../../../constants/ages');

var _ages2 = _interopRequireDefault(_ages);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (s) {
  return (0, _ramda.compose)((0, _ramda.prop)('stage'), (0, _ramda.find)((0, _ramda.compose)((0, _ramda.apply)(_ramda.and), (0, _ramda.ap)([(0, _ramda.compose)((0, _ramda.gt)((0, _ramda.prop)('age', s)), (0, _ramda.prop)('min')), (0, _ramda.compose)((0, _ramda.lt)((0, _ramda.prop)('age', s)), (0, _ramda.prop)('max'))]), _ramda.of)))(_ages2.default);
};