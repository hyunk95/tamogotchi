'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ramda = require('ramda');

var _state = require('../../../types/state');

var _state2 = _interopRequireDefault(_state);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var modify = _state2.default.modify;

exports.default = modify((0, _ramda.compose)((0, _ramda.when)((0, _ramda.compose)((0, _ramda.equals)('Death'), (0, _ramda.prop)('stage')), (0, _ramda.pick)(['stage']))));