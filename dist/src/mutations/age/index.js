'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ramda = require('ramda');

var _state = require('../../types/state');

var _state2 = _interopRequireDefault(_state);

var _initialState = require('../../constants/initial-state');

var _initialState2 = _interopRequireDefault(_initialState);

var _pickRelevantIfDead = require('./helpers/pick-relevant-if-dead');

var _pickRelevantIfDead2 = _interopRequireDefault(_pickRelevantIfDead);

var _updateState = require('./helpers/update-state');

var _updateState2 = _interopRequireDefault(_updateState);

var _updateAge = require('./helpers/update-age');

var _updateAge2 = _interopRequireDefault(_updateAge);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var age = (0, _ramda.compose)((0, _ramda.chain)((0, _ramda.always)(_pickRelevantIfDead2.default)), (0, _ramda.chain)((0, _ramda.always)(_updateState2.default)), (0, _ramda.chain)(_updateAge2.default), _state2.default.of);

exports.default = function (time) {
  return function () {
    var obj = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _initialState2.default;
    return age(time).exec(obj);
  };
};