'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ramda = require('ramda');

var _initialState = require('../../constants/initial-state');

var _initialState2 = _interopRequireDefault(_initialState);

var _updateState = require('./helpers/update-state');

var _updateState2 = _interopRequireDefault(_updateState);

var _state = require('../../types/state');

var _state2 = _interopRequireDefault(_state);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var setState = function setState(prop) {
  return (0, _ramda.compose)((0, _ramda.chain)((0, _updateState2.default)(prop)), _state2.default.of);
};

exports.default = function (state) {
  return function (prop) {
    return function () {
      var obj = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _initialState2.default;
      return setState(prop)(state).exec(obj);
    };
  };
};