'use strict';

var daggy = require('daggy');

var _require = require('fantasy-combinators'),
    constant = _require.constant,
    identity = _require.identity;

var Reader = daggy.tagged('run');

// Methods
Reader.of = function (a) {
    return Reader(constant(a));
};

Reader.ask = Reader(identity);

Reader.prototype.chain = function (f) {
    var _this = this;

    return Reader(function (e) {
        return f(_this.run(e)).run(e);
    });
};

// Derived
Reader.prototype.map = function (f) {
    return this.chain(function (a) {
        return Reader.of(f(a));
    });
};

Reader.prototype.ap = function (a) {
    return this.chain(function (f) {
        return a.map(f);
    });
};

// Transformer
Reader.ReaderT = function (M) {
    var ReaderT = daggy.tagged('run');

    ReaderT.lift = function (m) {
        return ReaderT(constant(m));
    };

    ReaderT.of = function (a) {
        return ReaderT(function (e) {
            return M.of(a);
        });
    };

    ReaderT.ask = ReaderT(function (e) {
        return M.of(e);
    });

    ReaderT.prototype.chain = function (f) {
        var _this2 = this;

        return ReaderT(function (e) {
            return _this2.run(e).chain(function (a) {
                return f(a).run(e);
            });
        });
    };

    ReaderT.prototype.map = function (f) {
        return this.chain(function (a) {
            return ReaderT.of(f(a));
        });
    };

    ReaderT.prototype.ap = function (a) {
        return this.chain(function (f) {
            return a.map(f);
        });
    };

    return ReaderT;
};

// Export
if (typeof module != 'undefined') module.exports = Reader;