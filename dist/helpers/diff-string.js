'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _ramda = require('ramda');

var _diffObj = require('../src/utils/diff-obj');

var _diffObj2 = _interopRequireDefault(_diffObj);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var keyRelevantString = function keyRelevantString(_ref) {
  var _ref2 = _slicedToArray(_ref, 2),
      key = _ref2[0],
      data = _ref2[1];

  return {
    'health': 'Health is now ' + data,
    'fullness': 'Fullness is now ' + data,
    'age': 'Age is now ' + data,
    'stage': 'Congratulations your pet is now ' + data,
    'state': 'Your pet is ' + data,
    'stoolCounter': 'Ewww your pet pooped',
    'isSleepy': 'Your pet is starting to get sleepy',
    'isHungry': 'YOUR PET IS HUNGRY FEED HIM/HER'
  };
};

exports.default = function (previousState) {
  return (0, _ramda.compose)((0, _ramda.join)('\n'), _ramda.values, (0, _ramda.mapObjIndexed)(function (num, key, obj) {
    return keyRelevantString(obj[key])[key];
  }), (0, _diffObj2.default)(previousState));
};