'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ramda = require('ramda');

exports.default = (0, _ramda.compose)((0, _ramda.join)('\n'), _ramda.values, (0, _ramda.mapObjIndexed)(function (num, key, obj) {
  return key + ': ' + obj[key];
}));