'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

// impure function
exports.default = function (line) {
  var completions = '.actions .status .quit .q'.split(' ');
  var hits = completions.filter(function (c) {
    if (c.indexOf(line) == 0) {
      return c;
    }
  });
  return [hits && hits.length ? hits : completions, line];
};