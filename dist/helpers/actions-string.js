'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ramda = require('ramda');

var _actions = require('../constants/actions');

var _actions2 = _interopRequireDefault(_actions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = (0, _ramda.compose)((0, _ramda.join)('\n'), (0, _ramda.map)(function (action) {
  return '[' + action.key + '] - ' + action.action;
}))(_actions2.default);