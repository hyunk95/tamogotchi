'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _fluture = require('fluture');

var _fluture2 = _interopRequireDefault(_fluture);

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (data) {
  return (0, _fluture2.default)(function (rej, res) {
    (0, _request2.default)(data, function (err, response, body) {
      err ? rej(err) : res(response.body);
    });
  });
};