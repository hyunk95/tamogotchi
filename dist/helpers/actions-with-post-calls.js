"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _futureRequest = require("./future-request");

var _futureRequest2 = _interopRequireDefault(_futureRequest);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (route) {
  return function (data) {
    return function (cb) {
      return (0, _futureRequest2.default)({
        url: "http://localhost:3000/" + route,
        method: "POST",
        json: data
      }).fork(console.error, cb);
    };
  };
};