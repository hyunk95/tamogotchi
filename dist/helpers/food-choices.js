'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ramda = require('ramda');

var _food = require('../src/utils/food');

exports.default = (0, _ramda.compose)((0, _ramda.zipObj)([1, 2, 3]), _ramda.keys)(_food.FOOD);