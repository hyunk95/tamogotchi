'use strict';

var _colors = require('colors');

var _colors2 = _interopRequireDefault(_colors);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var help = [_colors2.default.grey('.actions        ' + 'possible actions'), _colors2.default.grey('.status       ' + 'status of your pet'), _colors2.default.grey('.q[uit]      ' + 'exit console.')].join('\n');