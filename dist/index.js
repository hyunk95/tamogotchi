'use strict';

var _readline = require('readline');

var _readline2 = _interopRequireDefault(_readline);

var _colors = require('colors');

var _colors2 = _interopRequireDefault(_colors);

var _completer = require('./helpers/completer');

var _completer2 = _interopRequireDefault(_completer);

var _initialState = require('./src/constants/initial-state');

var _initialState2 = _interopRequireDefault(_initialState);

var _random = require('./src/utils/random');

var _random2 = _interopRequireDefault(_random);

var _getKeysAndValues = require('./helpers/get-keys-and-values');

var _getKeysAndValues2 = _interopRequireDefault(_getKeysAndValues);

var _isHeadPoint = require('./src/utils/predicate/is-head-point');

var _isHeadPoint2 = _interopRequireDefault(_isHeadPoint);

var _isCharacterInAlphabet = require('./src/utils/predicate/is-character-in-alphabet');

var _isCharacterInAlphabet2 = _interopRequireDefault(_isCharacterInAlphabet);

var _cron = require('cron');

var _actionsString = require('./helpers/actions-string');

var _actionsString2 = _interopRequireDefault(_actionsString);

var _help = require('./constants/help');

var _help2 = _interopRequireDefault(_help);

var _diffString = require('./helpers/diff-string');

var _diffString2 = _interopRequireDefault(_diffString);

var _actionsWithPostCalls = require('./helpers/actions-with-post-calls');

var _actionsWithPostCalls2 = _interopRequireDefault(_actionsWithPostCalls);

var _foodChoices = require('./helpers/food-choices');

var _foodChoices2 = _interopRequireDefault(_foodChoices);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var pet = _initialState2.default;

var rl = _readline2.default.createInterface(process.stdin, process.stdout, _completer2.default);

// impure function
function welcome() {
  console.log(["TAMAGOTCHI", "= Welcome, enter .help if you're lost."].join('\n').grey);

  prompt();
}

// impure function
function prompt() {
  var arrow = '> ',
      length = arrow.length;

  rl.setPrompt(_colors2.default.grey(arrow), length);
  rl.prompt();
}

new _cron.CronJob('0,10 * * * * *', function () {
  (0, _actionsWithPostCalls2.default)('check_status')({
    state: pet,
    seed: _random2.default.genCsSeed(),
    time: process.uptime()
  })(function (state) {
    console.log((0, _diffString2.default)(pet)(state));
    pet = state;
  });
}, null, true, 'America/Los_Angeles');

// impure function
function exec(command) {
  var int = parseInt(command);

  if ((0, _isHeadPoint2.default)(command)) {
    var action = command.slice(1);
    switch (action) {
      case 'help':
        console.log(_help2.default.yellow);
        break;
      case 'actions':
        console.log(_actionsString2.default.yellow);
        break;
      case 'status':
        console.log(pet);
        break;
      case 'exit':
        break;
      case 'q':
        process.exit(0);
        break;
    }
  }

  if (!isNaN(int)) {
    var val = _foodChoices2.default[int];

    !!val ? (0, _actionsWithPostCalls2.default)('feed')({
      state: pet,
      seed: _random2.default.genCsSeed(),
      time: process.uptime(),
      foodChoice: val
    })(function (state) {
      pet = state;
      console.log('Your pet was fed!');
      prompt();
    }) : console.log('Invalid Food Choice');
  }

  if ((0, _isCharacterInAlphabet2.default)(command)) {
    switch (command) {
      case 'a':
        (0, _actionsWithPostCalls2.default)('sleep')({
          state: pet,
          seed: _random2.default.genCsSeed(),
          time: process.uptime()
        })(function (state) {
          pet = state;
          console.log('Your pet is now asleep');
          prompt();
        });
        break;
      case 'b':
        console.log('What would you like to feed it?');
        console.log((0, _getKeysAndValues2.default)(_foodChoices2.default));
        break;
    }
  }

  prompt();
}

rl.on('line', function (cmd) {
  exec(cmd.trim());
}).on('close', function () {
  console.log('goodbye!'.green);
  process.exit(0);
});

welcome();